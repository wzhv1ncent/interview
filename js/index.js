;(function(win, $, echarts) {
  function getDate() {
    var today = new Date()
    var dd = today.getDate()
    var mm = today.getMonth() + 1
    var yyyy = today.getFullYear()

    if (dd < 10) {
      dd = '0' + dd
    }

    if (mm < 10) {
      mm = '0' + mm
    }

    today = mm + '/' + dd + '/' + yyyy

    return today
  }

  function renderChartTime() {
    var myChart = echarts.init(document.getElementById('echart-time'))

    var option = {
      tooltip: {},
      xAxis: {
        data: ['8:30-9:30', '9:30-10:30', '10:30-11:30', '11:30-12:30']
      },
      yAxis: {},
      series: [
        {
          name: '人数',
          type: 'line',
          data: [5, 5, 10, 15]
        }
      ]
    }

    myChart.setOption(option)
  }

  function renderChartDay() {
    var myChart = echarts.init(document.getElementById('echart-day'))

    var option = {
      tooltip: {},
      xAxis: {
        data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
      },
      yAxis: {},
      series: [
        {
          name: '人数',
          type: 'bar',
          data: [100, 80, 60, 80, 70, 30, 20]
        }
      ]
    }

    myChart.setOption(option)
  }

  $(win).load(function() {
    renderChartTime()
    renderChartDay()

    $('.sym-time').text(getDate())

    $('.carousel-2').owlCarousel({
      margin: 10,
      loop: true,
      items: 4,
      nav: true,
      autoplay: true
    })

    $.get('/api/imgs', function(response) {
      var carousel = response.data

      carousel.forEach(function(item) {
        $('.carousel-1').append('<div class="item"><img src="' + item.image + '" ></div>')
      })

      $('.carousel-1').owlCarousel({
        loop: true,
        nav: true,
        dots: false,
        items: 1,
        center: true,
        autoplay: true
      })
    })

    $.get('/api/index', function(response) {
      var affiche = {
        top: response.data.affiche.top,
        list: response.data.affiche.list
      }

      var policy = response.data.policy

      var tip = response.data.tip

      $('.affiche-top-title').text(affiche.top.title)
      $('.affiche-top-detail').text(affiche.top.detail)

      affiche.list.forEach(function(item) {
        $('.affiche-list').append(
          '<li class="clearfix"><span class="fl">' +
            item.title +
            '</span><span class="fr">' +
            item.date +
            '</span></li>'
        )
      })

      policy.forEach(function(item) {
        $('.policy').append(
          '<li class="clearfix"><span class="fl">' +
            item.title +
            '</span><span class="fr">' +
            item.date +
            '</span></li>'
        )
      })

      tip.forEach(function(item) {
        $('.tip').append(
          '<li class="clearfix"><span class="fl">' +
            item.title +
            '</span><span class="fr">' +
            item.date +
            '</span></li>'
        )
      })
    })
  })
})(this, jQuery, echarts)
