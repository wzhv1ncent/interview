const express = require('express')
const { resolve } = require('path')
const { port, host } = require('./config')
const router = require('./router')

const app = express()

router(app)

app.use('/js', express.static(resolve(__dirname, '../js')))
app.use('/css', express.static(resolve(__dirname, '../css')))
app.use('/images', express.static(resolve(__dirname, '../images')))
app.use('/vendor', express.static(resolve(__dirname, '../vendor')))

app.listen(port, host, err => {
  err ? console.error(err) : console.log(`server listening on ${host}:${port}`)
})
