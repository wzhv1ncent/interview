const fs = require('fs')

module.exports = (res, file) => {
  fs.readFile(file, 'utf8', (err, file) => {
    if (err) {
      res.json({
        status: 404,
        msg: err.toString()
      })
    } else {
      res.json({
        status: 200,
        data: JSON.parse(file)
      })
    }
  })
}
