const { resolve } = require('path')
const sendFile = require('../utils/sendFile')
const readJSON = require('../utils/readJSON')

module.exports = app => {
  app.get('/', (req, res) => {
    sendFile(res, resolve(__dirname, '../../index.html'))
  })

  app.get('/api/imgs', (req, res) => {
    readJSON(res, resolve(__dirname, '../data/imgs.json'))
  })

  app.get('/api/index', (req, res) => {
    readJSON(res, resolve(__dirname, '../data/index.json'))
  })
}
